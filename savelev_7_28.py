"""
Савельев Александр КИ22-16\1Б
Вариант 28.
Разработайте программу для хранения данных о сотрудниках в
организации.
Организация состоит из нескольких отделов, в каждом из которых
работают несколько сотрудников на разных должностях.
Разработайте и реализуйте диаграмму классов и все необходимые
методы, и свойства.
Задания

"""
import sys
import os
import pickle


class Department:
    """
        A class to represent a department

        ...

        Attributes
        ----------
        name : str
            name of the department

        Methods
        -------
        total_salary():
            Prints the total salary of all employees in the department

        print_employees():
            Prints all employees working in the department

        add_employee(employee):
            Adding a new employee in department

        remove_employee(employee_id):
            Removing an employee by his id
        """
    foo = 9

    def __init__(self, name):
        self.name = name
        self.employees = []

    def total_salary(self) -> int:
        """
        Counting total salary of all employees
        :return: total salary
        """
        salary_sum = 0
        for person in self.employees:
            salary_sum += person.salary
        return salary_sum

    def print_employees(self):
        """
        Printing all info about employees in department
        """
        for id, person in enumerate(self.employees):
            print(f'{id}--{person.name}--{person.salary}')

    def add_employee(self, employee):
        """
        Adding a new employee
        :param employee: Object of employee.
        """
        self.employees.append(employee)

    def remove_employee(self, employee_id):
        """
        Deleting an employee by his id
        :param employee_id: id of employee.
        """
        self.employees.pop(employee_id)


class Employee:
    """
     A class to represent an Employee

        ...

        Attributes
        ----------
        name : str
            name of the employee

        salary : int
            the amount of the employee salary
    """

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary


class Programmer(Employee):
    """
    A class to represent a Programmer

    ...

    Attributes
    ----------
    Methods
    -------
    work():
        Some kind of a work

    """

    def __init__(self, name, salary):
        super().__init__(name, salary)

    def work(self):
        """
        Do something
        """
        print(f'{self.name} is coding something')


class Manager(Employee):
    """
     A class to represent a Manager

     ...

     Attributes
     ----------
     Methods
     -------
     work():
         Some kind of a work

     """

    def __init__(self, name, salary):
        super().__init__(name, salary)

    def work(self):
        """
        Do something
        """
        print(f'{self.name} is managing something')


class Cleaner(Employee):
    """
     A class to represent a Cleaner

     ...

     Attributes
     ----------
     Methods
     -------
     work():
         Some kind of a work

     """

    def __init__(self, name, salary):
        super().__init__(name, salary)

    def work(self):
        """
        Do something
        """
        print(f'{self.name} is cleaning floor ')


def add_employee(department):
    """
    Function for adding a new employee to department.
    Function include checking all necessary variables.
    :param department: object of department
    :return:
    """
    while True:
        employee_class = input('Введите класс работника: ')
        if employee_class in [cls.__name__ for cls in Employee.__subclasses__()]:
            break
        else:
            print('Класс не существует, повторите попытку')
    employee_data = dict()
    employee_data['name'] = input('Введите имя работника: ')
    while True:
        try:
            employee_data['salary'] = int(input('Введите зарплату работника: '))
            break
        except ValueError:
            print('Зарплата должна быть целым числом')
    department.add_employee(globals()[employee_class](*employee_data.values()))


MENU = {'1': ("Просмотреть все отделы",),
        '2': ("Посчитать общую зп сотрудников в отделе",),
        '3': ("Добавить сотрудника",),
        '4': ("Удалить сотрудника",),
        '5': ("Добавить отдел",),
        '6': ("Удалить отдел",),
        '7': ("Загрузить данные из файла",),
        '8': ("Сохранить наработки в файл",),
        '9': ("Просмотр доступных классов работников",),
        '10': ("Добавить новый класс работника",),
        '11': ("Выход",)
        }


def print_menu():
    """
    Function that prints menu in pretty view for user
    """
    for number, command in MENU.items():
        print(f'{number}: {command[0]}')


def select_command() -> str:
    """
    Function for correct input a command from menu
    :return: user_answer
    """
    while True:
        user_answer = input('Введите номер команды: ')
        if user_answer in MENU:
            return user_answer
        else:
            print('Неверный номер команды')
            print_menu()


def input_id(company):
    """
    Function for correct input an id
    :param company: list of all departments in company
    :return: False : if given id does not exist
             user_department_id : if given id  in the list
    """
    if len(company) == 0:
        return False
    else:
        while True:
            user_department_id = input('Введите id: ')
            try:
                if user_department_id.isdigit() and company[int(user_department_id)]:
                    return int(user_department_id)
            except IndexError:
                print('Неверный id')
                print_menu()


def create_new_class(new_class_name):
    """
    Function for creating new class with name, that user input
    :param new_class_name: name of the new class
    :return: new Class
    """

    def init(self, name, salary):
        self.name = name
        self.salary = salary

    def work(self):
        print(f"Hi, I'm {self.name} and I'm working right now")

    return type(new_class_name, (Employee,), {
        '__init__': init,
        'work': work,
    })


def main():
    company = list()
    while True:
        os.system('pause')
        os.system('cls')
        print_menu()

        # user_answer = select_command()
        user_answer = input('Введите номер команды: ')
        match user_answer:
            case '1':
                for id, department in enumerate(company):
                    print(f'Работники отдела {id}--{department.name}: ')
                    department.print_employees()
            case '2':
                user_department_id = input_id(company)
                if type(user_department_id) == int:
                    print(company[int(user_department_id)].total_salary())
            case '3':
                print('Отдел:')
                user_department_id = input_id(company)
                if type(user_department_id) == int:
                    add_employee(company[user_department_id])
                    print('Добавлен новый работник')
            case '4':
                print('Отдел:')
                user_department_id = input_id(company)
                if type(user_department_id) == int:
                    print('Рабочий:')
                    employee_id = input_id(company[user_department_id].employees)
                    if type(employee_id) == int:
                        company[user_department_id].remove_employee(employee_id)
                        print(f'Удален работник_{employee_id}')
            case '5':
                company.append(Department(input('Введите название отдела: ')))
                print('Добавлен новый отдел')
            case '6':
                user_department_id = input_id(company)
                company.pop(user_department_id)
                print(f'Отдел {user_department_id} удален')
            case '7':
                with open('data.pickle', 'rb') as file:
                    company = pickle.load(file)
                    print('Данные загружены')
            case '8':
                with open('data.pickle', 'wb') as file:
                    pickle.dump(company, file)
                    print('Данные сохранены')
            case '9':
                print(*[cls.__name__ for cls in Employee.__subclasses__()], sep=' ')
            case '10':
                new_class_name = input('Введите название нового класса: ')
                globals()[new_class_name] = create_new_class(new_class_name)
                print('Добавлен новый класс')
            case '11':
                sys.exit()
            case _:
                print('Неверный номер команды')


if __name__ == "__main__":
    main()
